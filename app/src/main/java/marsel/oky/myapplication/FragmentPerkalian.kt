package marsel.oky.myapplication

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_perkalian.view.*
import kotlinx.android.synthetic.main.frag_perkalian.*
import java.text.DecimalFormat

class FragmentPerkalian : Fragment(), View.OnClickListener {

    lateinit var thisParent : MainActivity
    lateinit var v : View
    var x : Double = 0.0
    var y : Double = 0.0
    var hasil : Double = 0.0

    override fun onClick(v: View?) {
        x = edX1.text.toString().toDouble()
        y = edX1.text.toString().toDouble()
        hasil = x*y
        txHasilKali.text = DecimalFormat("#.##").format(hasil)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_perkalian,container,false)
        v.btnKali.setOnClickListener(this)
        return v
    }
}